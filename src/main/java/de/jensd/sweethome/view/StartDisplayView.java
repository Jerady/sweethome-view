/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.sweethome.view;

import com.guigarage.responsive.DeviceType;
import com.guigarage.responsive.ResponsiveHandler;
import java.util.Locale;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class StartDisplayView extends Application {

    private static final Logger LOGGER = LogManager.getLogger(StartDisplayView.class);

    @Override
    public void init() throws Exception {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Override
    public void start(Stage primaryStage) {
        System.out.print("Starting Sweethome Display. Please wait....");
        DisplayView displayView = new DisplayView();
        Scene scene = new Scene(displayView, Color.BLACK);
        String css = getClass().getResource("display.css").toExternalForm();
        scene.getStylesheets().addAll(css);
        primaryStage.setTitle("SweetHome Display");
        primaryStage.setScene(scene);
        primaryStage.show();
        displayView.onConnect();
        System.out.println("OK.");
        ResponsiveHandler.addResponsiveToWindow(primaryStage);

        ResponsiveHandler.setOnDeviceTypeChanged(new ChangeListener<DeviceType>() {
            @Override
            public void changed(ObservableValue<? extends DeviceType> observable, DeviceType oldValue, DeviceType newValue) {
                System.out.println(newValue);
            }
        });
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
