/*
 * Copyright (c) 2015, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.view;

import de.jensd.fx.control.ValueView;
import de.jensd.sweethome.core.MqttClientConfiguration;
import de.jensd.sweethome.core.SweetHomeValues;
import de.jensd.sweethome.core.SweethomeTopics;
import de.jensd.sweethome.core.mqtt.SweethomeValuesMQTTAdapter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.VBox;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author Jens Deters
 */
public class DisplayView extends VBox {

    @FXML
    private Label pressureValueLabel;
    @FXML
    private Label humidityValueLabel;
    @FXML
    private Label lastUpdateLabel;
    @FXML
    private Label luxVisibleValueLabel;
    @FXML
    private Label luxInfraredValueLabel;
    @FXML
    private TabPane mainTabPane;
    @FXML
    private ToggleButton displayModeToggleButton;
    @FXML
    private ValueView indoorTempView;
    @FXML
    private ValueView outdoorTempView;
    @FXML
    private ValueView luxVisibleView;
    @FXML
    private ValueView luxInfraredView;
    @FXML
    private ValueView humidityView;
    @FXML
    private ValueView pressureView;

    @FXML
    private ResourceBundle resources;

    private SweethomeValuesMQTTAdapter valuesMQTTAdapter;
    private SweetHomeValues valuesDataModel;

    public DisplayView() {
        init();
    }

    private void init() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(getClass().getPackage().getName() + ".display");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("display.fxml"));
        fxmlLoader.setResources(resourceBundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        indoorTempView.valueProperty().bind(getValuesDataModel().temperatureIndoorProperty().asString("%.1f"));
        outdoorTempView.valueProperty().bind(getValuesDataModel().temperatureOutdoorProperty().asString("%.1f"));
        luxVisibleView.valueProperty().bind(getValuesDataModel().luxVisibleProperty().asString("%d"));
        luxInfraredView.valueProperty().bind(getValuesDataModel().luxInfraredProperty().asString("%d"));
        humidityView.valueProperty().bind(getValuesDataModel().humidityProperty().asString("%.1f"));
        pressureView.valueProperty().bind(getValuesDataModel().pressureProperty().asString("%.2f"));
        valuesDataModel.lastUpdateProperty().addListener((ObservableValue<? extends LocalDateTime> observable, LocalDateTime oldValue, LocalDateTime newValue) -> {
            lastUpdateLabel.setText(DateTimeFormatter.ofPattern("HH:mm:ss").format(newValue));
        });
    }
    /*
     * -------------------------- ACTIONS -------------------------- 
     */

    @FXML
    public void onExit() {
        onDisconnect();
        Platform.exit();
        System.exit(0);
    }

    public SweetHomeValues getValuesDataModel() {
        if (valuesDataModel == null) {
            valuesDataModel = new SweetHomeValues();
        }
        return valuesDataModel;
    }

    public SweethomeValuesMQTTAdapter getValuesMQTTAdapter() {
        if (valuesMQTTAdapter == null) {
            MqttClientConfiguration configuration = new MqttClientConfiguration();
            configuration.setMqttBrokerAddress("192.168.0.61");
            configuration.setMqttBrokerPort("1883");
            configuration.setMqttClientId(UUID.randomUUID().toString());
            valuesMQTTAdapter = new SweethomeValuesMQTTAdapter(configuration, valuesDataModel);
        }
        return valuesMQTTAdapter;
    }

    @FXML
    public void onReConnect() {
        try {
            getValuesMQTTAdapter().reconnect();
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void onConnect() {
        try {
            getValuesMQTTAdapter().connect();
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void onDisconnect() {
        if (getValuesMQTTAdapter() != null && getValuesMQTTAdapter().isClientConnected()) {
            try {
                getValuesMQTTAdapter().disconnect();
            } catch (MqttException ex) {
                ex.printStackTrace();
            }
        }
    }

    @FXML
    public void onToggleDisplayMode() {
        if (displayModeToggleButton.isSelected()) {
            setOpacity(1.0);
            displayModeToggleButton.setText("Day");
        } else {
            setOpacity(0.1);
            displayModeToggleButton.setText("Night");
        }

    }

    @FXML
    public void onNextTab() {
        mainTabPane.getSelectionModel().selectNext();
    }

    @FXML
    public void onPreviousTab() {
        mainTabPane.getSelectionModel().selectPrevious();
    }
    
    private void doPublish(String topic, String payload){
        if(!getValuesMQTTAdapter().isClientConnected()){
            onConnect();
        }
        getValuesMQTTAdapter().publish(topic, payload);
    }

    @FXML
    public void onSwitchOnFrontLightAmbiente() {
        doPublish(SweethomeTopics.FRONT_LIGHT_AMBIENTE.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffFrontLightAmbiente() {
        doPublish(SweethomeTopics.FRONT_LIGHT_AMBIENTE.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnFrontLightDoor() {
        doPublish(SweethomeTopics.FRONT_LIGHT_DOOR.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffFrontLightDoor() {
        doPublish(SweethomeTopics.FRONT_LIGHT_DOOR.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnGardenLightTerrace() {
        doPublish(SweethomeTopics.BACKYARD_LIGHT_TERRACE.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffGardenLightTerrace() {
        doPublish(SweethomeTopics.BACKYARD_LIGHT_TERRACE.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnGardenLightAmbiente() {
        doPublish(SweethomeTopics.BACKYARD_LIGHT_AMBIENTE.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffGardenLightAmbiente() {
        doPublish(SweethomeTopics.BACKYARD_LIGHT_AMBIENTE.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnLivingRoomLightAmbiente() {
        doPublish(SweethomeTopics.LIVINGROOM_LIGHT_AMBIENTE.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffLivingRoomLightAmbiente() {
        doPublish(SweethomeTopics.LIVINGROOM_LIGHT_AMBIENTE.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnGardenDeviceFountain() {
        doPublish(SweethomeTopics.BACKYARD_DEVICE_FOUNTAIN.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffGardenDeviceFountain() {
        doPublish(SweethomeTopics.BACKYARD_DEVICE_FOUNTAIN.getNameWithDefaultBaseTopic(), "OFF");
    }

}
